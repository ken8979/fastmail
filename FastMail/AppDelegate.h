//
//  AppDelegate.h
//  FastMail
//
//  Created by suzuki on 2015/01/01.
//  Copyright (c) 2015年 GridStitch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

