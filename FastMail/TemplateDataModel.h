#import <Foundation/Foundation.h>

@interface TemplateDataModel : NSObject

@property (nonatomic) NSMutableArray *templateData;

//ID
@property (nonatomic) NSNumber *id;

//TOメールアドレス
@property (nonatomic) NSString *mailAddr;

//並び順　昇順
@property (nonatomic) NSNumber *order;

//本文
@property (nonatomic) NSString *body;

//タイトル
@property (nonatomic) NSString *subject;

//タグ
@property (nonatomic) NSString *tag;

- (NSArray *)templateKeys;

- (instancetype)initWithPList;

- (instancetype)getTemplateFromPList;


@end