//
//  UITableViewController+MailActionListView.h
//  FastMail
//
//  Created by suzuki on 2015/01/01.
//  Copyright (c) 2015年 GridStitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import "TemplateDataModel.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface MailActionListView : UITableViewController <MFMailComposeViewControllerDelegate>
{
    NSMutableArray *templateData;
    NSMutableArray *_objects;
    NSArray *_textArray;    // 追加
    CustomCell *_stubCell;  // 追加
}


@end
