//
//  FirstViewController.m
//  FastMail
//
//  Created by suzuki on 2015/01/01.
//  Copyright (c) 2015年 GridStitch. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (IBAction)tapSendMailButton:(id)sender {
    [self sendMail];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendMail{
    // メールを送信することができるか確認
    if ([MFMailComposeViewController canSendMail] == NO) {
        NSLog(@"%s can't send mail", __PRETTY_FUNCTION__);
        return;
    }
    
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    // メール件名
    [controller setSubject:@"メールの件名をここに入力します"];
    
    // メール本文
    [controller setMessageBody:@"メールの本文がここに入ります" isHTML:NO];
    
    // メールビュー表示
    [self presentViewController:controller animated:YES completion:nil];
    return;
}
@end
