    //
//  UITableViewController+MailActionListView.m
//  FastMail
//
//  Created by suzuki on 2015/01/01.
//  Copyright (c) 2015年 GridStitch. All rights reserved.
//

#import "MailActionListView.h"


@interface MailActionListView ()<UITableViewDelegate, UITableViewDataSource> // テーブルビューのプロトコルに準拠します

/**
 Storyboardに配置したテーブルが紐づいてます
 */
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation MailActionListView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    // テーブルに表示したいデータソースをセット
    
    TemplateDataModel *tmp =  [[TemplateDataModel alloc]initWithPList];
    templateData = tmp.templateData;
    
    //NSLog(@"template3:%@",tmp.templateData);
    NSLog(@"template3:%@",templateData);
}

#pragma mark - UITableView DataSource

/**
 テーブルに表示するデータ件数を返します。（必須）
 
 @return NSInteger : データ件数
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger dataCount;
    
    // テーブルに表示するデータ件数を返す
    switch (section) {
        case 0:
            dataCount = [templateData count];
            break;
        default:
            break;
    }
    return dataCount;
}

/**
 テーブルに表示するセクション（区切り）の件数を返します。（オプション）
 
 @return NSInteger : セクションの数
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

/**
 テーブルに表示するセルを返します。（必須）
 
 @return UITableViewCell : テーブルセル
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCell";
    // 再利用できるセルがあれば再利用する
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    [self configureCell:cell atIndexPath:indexPath];    // 追加
    
    if (!cell) {
        // 再利用できない場合は新規で作成
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
    }
    
    switch (indexPath.section) {
        case 0:
            //cell.textLabel.text = templateData[indexPath.row][@"body"];
            break;
        default:
            break;
    }
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CustomCell *customCell = (CustomCell *)cell;
    
    // メインラベルに文字列を設定
    customCell.mainLabel.text = templateData[indexPath.row][@"body"];
    
    // サブラベルに文字列を設定
    customCell.subLabel.text = templateData[indexPath.row][@"subject"];
    
    customCell.mailAddrLabel.text = templateData[indexPath.row][@"mailAddr"];
}

/**
 テーブルタップした時の挙動
 @return UITableViewCell : テーブルセル
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self sendMail:templateData[indexPath.row][@"subject"] :templateData[indexPath.row][@"body"]];
    
}

- (void)sendMail:(NSString*)subject :(NSString*)body{
    // メールを送信することができるか確認
    if ([MFMailComposeViewController canSendMail] == NO) {
        NSLog(@"%s can't send mail", __PRETTY_FUNCTION__);
        return;
    }
    
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    // メール件名
    [controller setSubject:subject];
    
    // 宛先（TO）を設定する
    //[controller setToRecipients:[NSArray arrayWithObject:@"ken8979@gmail.com"]];
    NSArray *toRecipients = [NSArray arrayWithObject:@"ken8979@gmail.com"];
    [controller setToRecipients:toRecipients];
    
    // メール本文
    [controller setMessageBody:@"メールの本文がここに入ります" isHTML:NO];
    
    // 本文を設定する
    NSString *messageBody = @"No Cat, No Life";
    [controller setMessageBody:messageBody isHTML:NO];
    
    // メールビュー表示
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
    
    // resultに結果が入ってくる
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"%s Cancelled", __PRETTY_FUNCTION__);
            break;
        case MFMailComposeResultSaved:
            NSLog(@"%s Saved", __PRETTY_FUNCTION__);
            break;
        case MFMailComposeResultSent:
            NSLog(@"%s Sent", __PRETTY_FUNCTION__);
            break;
        case MFMailComposeResultFailed:
            NSLog(@"%s Failed", __PRETTY_FUNCTION__);
            break;
        default:
            break;
    }
    
    // モーダルビューを閉じる
    [self dismissViewControllerAnimated:YES completion:nil];
}






@end
