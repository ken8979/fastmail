//
//  TemplateDataModel.m
//  FastMail
//
//  Created by suzuki on 2015/01/01.
//  Copyright (c) 2015年 GridStitch. All rights reserved.
//

#import "TemplateDataModel.h"


@implementation TemplateDataModel

- (NSArray *)templateKeys
{
    return @[@"id", @"mailAddr", @"order", @"body", @"subject", @"tag"];
}

- (instancetype)initWithPList
{
    self = [super init];
    if (self) {
        [self getTemplateFromPList];
        [self.templateData addObject:@"test"];
        [self saveToPlistWithArray:self.templateData];
        [self getTemplateFromPList];
    }
    return self;
}


- (void)saveToPlistWithArray:(NSArray *)array
{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"TemplateDataStore" ofType:@"plist"];

    //Cacheディレクトリ
    NSString *cachePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"TemplateDataCache.plist"];
    
    NSFileManager *filemanager = [NSFileManager defaultManager];
    if (![filemanager fileExistsAtPath:cachePath]) {
        [filemanager copyItemAtPath:path toPath:cachePath error:nil];
    }
    
    BOOL successful = [array writeToFile:cachePath atomically:NO];
    
    if (successful) {
        NSLog(@"%@", array);
        
    }

}

- (instancetype)getTemplateFromPList
{
    //plistを読み込む
    NSString *path = [[NSBundle mainBundle] pathForResource:@"TemplateDataStore" ofType:@"plist" ];
    NSData *plistData = [NSData dataWithContentsOfFile:path];
    NSPropertyListFormat format = NSPropertyListXMLFormat_v1_0;
    NSError *error;
    
    //plistをオブジェクトに変換
    id templates = [NSPropertyListSerialization propertyListWithData:plistData options:(NSPropertyListReadOptions)NSPropertyListImmutable format:&format error:&error];
    
    if (!templates) {
        //エラーハンドリング　気が向いたらなんか書く
    }
    
    self.templateData = [NSMutableArray array];

    for (NSDictionary *template in templates) {
        
        [self setId:template[@"id"]];
        [self setMailAddr:template[@"mailAddr"]];
        [self setBody:template[@"body"]];
        [self setOrder:template[@"order"]];
        [self setSubject:template[@"subject"]];
        [self setTag:template[@"tag"]];
        [self.templateData addObject:template];

        //NSLog(@"tamplate==:%@", tamplate);
    }
    NSLog(@"self.templateData:%@",self.templateData);

    return templates;
}

@end
