//
//  CustomCell.h
//  FastMail
//
//  Created by suzuki on 2015/01/03.
//  Copyright (c) 2015年 GridStitch. All rights reserved.

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;
@property (weak, nonatomic) IBOutlet UILabel *mailAddrLabel;

@end