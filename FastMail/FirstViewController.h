//
//  FirstViewController.h
//  FastMail
//
//  Created by suzuki on 2015/01/01.
//  Copyright (c) 2015年 GridStitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface FirstViewController : UIViewController <MFMailComposeViewControllerDelegate> 
@end

